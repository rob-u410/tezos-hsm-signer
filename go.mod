module gitlab.com/polychain/tezos-hsm-signer

require (
	github.com/aws/aws-sdk-go v1.19.1
	github.com/btcsuite/btcutil v0.0.0-20190316010144-3ac1210f4b38
	github.com/miekg/pkcs11 v0.0.0-20190322140431-074fd7a1ed19
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c
	golang.org/x/sys v0.0.0-20190322080309-f49334f85ddc // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
